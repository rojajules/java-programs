package com.framework;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ArrayUpdateFrame extends JFrame{
	private JTextField ArrayText;
	public ArrayList<String> al;
	public ArrayUpdateFrame() {
		getContentPane().setLayout(null);
		
		ArrayText = new JTextField();
		ArrayText.setFont(new Font("Tahoma", Font.BOLD, 20));
		ArrayText.setBounds(273, 80, 161, 37);
		getContentPane().add(ArrayText);
		ArrayText.setColumns(10);
		al=new ArrayList<String>();
		
		
		JButton ButtonAdd = new JButton("Add");
		ButtonAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e1) {
				
					 if (e1.getSource() == ButtonAdd) {
						 al.add(ArrayText.getText());
				           JOptionPane.showInputDialog(this, "Item inserted"+al);       
				}
			}
		});
		ButtonAdd.setFont(new Font("Tahoma", Font.BOLD, 20));
		ButtonAdd.setBounds(58, 173, 96, 37);
		getContentPane().add(ButtonAdd);
		
		JButton btnDel = new JButton("Del");
		btnDel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e1) {
					 if (e1.getSource() == btnDel) {
						 al.remove(ArrayText.getText());
				           JOptionPane.showInputDialog(this, "Item Removed"+al);        
				}
			}
		});
	
		btnDel.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnDel.setBounds(213, 173, 96, 37);
		getContentPane().add(btnDel);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e1) {
					 if (e1.getSource() == btnUpdate) {
						 String itm=ArrayText.getText();
				           int ind=al.indexOf(itm);
				           if(al.contains(itm))
				           {
				           String newitm=JOptionPane.showInputDialog(this,"Enter new value");
				           al.set(ind, newitm);
				           JOptionPane.showInputDialog(this, itm+" Item Updated"+al);  
				           }
				           else
				        	   JOptionPane.showInputDialog(this, itm+" Item is not found");  
					 }
				}
		});
		btnUpdate.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnUpdate.setBounds(373, 173, 113, 37);
		getContentPane().add(btnUpdate);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e1) {
					 if (e1.getSource() == btnSearch) {
						 String itm=ArrayText.getText();
				           if(al.contains(itm))
				           JOptionPane.showInputDialog(this, itm+" Item is found");
				           else
				        	   JOptionPane.showInputDialog(this, itm+" Item is not found");  
					 }
			}
		});
		btnSearch.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnSearch.setBounds(527, 173, 113, 37);
		getContentPane().add(btnSearch);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayUpdateFrame frame = new ArrayUpdateFrame();
        frame.setTitle("Login Form");
        frame.setVisible(true);
        frame.setBounds(10, 10, 700, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);

	}

}
