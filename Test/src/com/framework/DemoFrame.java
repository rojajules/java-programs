package com.framework;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

public class DemoFrame extends JFrame{
	private JTextField usertext;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;
	private JPasswordField passwordField_2;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	public DemoFrame() {
		getContentPane().setLayout(null);
		
		JLabel jl = new JLabel("Enter UserName");
		jl.setFont(new Font("Tahoma", Font.BOLD, 20));
		jl.setBounds(28, 30, 172, 31);
		getContentPane().add(jl);
		
		usertext = new JTextField();
		usertext.setBounds(225, 30, 145, 29);
		getContentPane().add(usertext);
		usertext.setColumns(10);
		
		JLabel pl = new JLabel("Enter Password");
		pl.setFont(new Font("Tahoma", Font.BOLD, 20));
		pl.setBounds(28, 86, 172, 31);
		getContentPane().add(pl);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(375, 114, -150, -37);
		getContentPane().add(passwordField);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(269, 114, -47, 19);
		getContentPane().add(passwordField_1);
		
		passwordField_2 = new JPasswordField();
		passwordField_2.setBounds(225, 86, 145, 31);
		getContentPane().add(passwordField_2);
		
		JButton btnNewButton = new JButton("Submit");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 10));
		btnNewButton.setBounds(166, 213, 85, 21);
		getContentPane().add(btnNewButton);
		
		JComboBox Languages = new JComboBox();
		Languages.setFont(new Font("Tahoma", Font.BOLD, 12));
		Languages.setModel(new DefaultComboBoxModel(new String[] {"tamil", "telugu", "english"}));
		Languages.setBounds(55, 125, 167, 21);
		getContentPane().add(Languages);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("reading");
		chckbxNewCheckBox.setFont(new Font("Tahoma", Font.BOLD, 12));
		chckbxNewCheckBox.setBounds(65, 152, 93, 21);
		getContentPane().add(chckbxNewCheckBox);
		
		JCheckBox chckbxNewCheckBox_1 = new JCheckBox("swimming");
		chckbxNewCheckBox_1.setFont(new Font("Tahoma", Font.BOLD, 12));
		chckbxNewCheckBox_1.setBounds(181, 152, 93, 21);
		getContentPane().add(chckbxNewCheckBox_1);
		
		JCheckBox chckbxNewCheckBox_2 = new JCheckBox("writing");
		chckbxNewCheckBox_2.setFont(new Font("Tahoma", Font.BOLD, 12));
		chckbxNewCheckBox_2.setBounds(305, 152, 93, 21);
		getContentPane().add(chckbxNewCheckBox_2);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Male");
		buttonGroup.add(rdbtnNewRadioButton);
		rdbtnNewRadioButton.setFont(new Font("Tahoma", Font.BOLD, 12));
		rdbtnNewRadioButton.setBounds(58, 186, 103, 21);
		getContentPane().add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Female");
		buttonGroup.add(rdbtnNewRadioButton_1);
		rdbtnNewRadioButton_1.setFont(new Font("Tahoma", Font.BOLD, 12));
		rdbtnNewRadioButton_1.setBounds(213, 186, 103, 21);
		getContentPane().add(rdbtnNewRadioButton_1);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DemoFrame df=new DemoFrame();
		df.setTitle("login screen");
		df.setVisible(true);
		df.setBounds(10,10,10,10);

	}
}
