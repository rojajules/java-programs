package com.framework;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Demo2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		JFrame f1=new JFrame("First Frame");
		f1.setLayout(new GridLayout(3,1));
		JLabel l1=new JLabel("this is my first label");
		l1.setSize(350,100);
		l1.setBackground(Color.RED);
		
		final JTextField tf=new JTextField();
		tf.setBounds(50, 100, 150, 20);
		tf.setText("before clicking");
		JButton b1=new JButton("clickme");
		b1.setBounds(50, 100,95, 30);
		b1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				tf.setText("hello welcome");
			}
			
		});
				f1.add(b1);
				f1.add(tf);
				f1.setSize(400,400);
				f1.setVisible(true);
	}

}
