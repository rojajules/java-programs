package com.framework;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class ArrayFrameNew extends JFrame {
	private JTextField ArrayText;
	public ArrayList<String> al;
	public int count;
	public ArrayFrameNew() {
		getContentPane().setLayout(null);
		
		ArrayText = new JTextField();
		ArrayText.setFont(new Font("Tahoma", Font.BOLD, 16));
		ArrayText.setBounds(287, 53, 96, 26);
		getContentPane().add(ArrayText);
		ArrayText.setColumns(10);
		al=new ArrayList<String>();
		al.add("first");
		al.add("second");
		al.add("third");
		al.add("fourth");
		al.add("fifth");
		
		JButton PreviousButton = new JButton("previous");
		PreviousButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e1) {
				if(e1.getSource() == PreviousButton) {
					 if (e1.getSource() == PreviousButton) {
						 PreviousButton.setEnabled(true);   
						 String itm=ArrayText.getText();
				            String prev=al.get(al.indexOf(itm)-1);
				            ArrayText.setText( prev);
				            if(prev==al.get(0))
				            {
				            ArrayText.setText( prev);
				          PreviousButton.setEnabled(false);
				            }
				            else
				            	ArrayText.setText( prev);
				}
					
				}
			}
		});
		PreviousButton.setFont(new Font("Tahoma", Font.BOLD, 20));
		PreviousButton.setBounds(209, 137, 133, 39);
		getContentPane().add(PreviousButton);
		
		JButton NextButton = new JButton("Next");
		NextButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(e.getSource() == NextButton) {
					PreviousButton.setEnabled(true);
					 String itm=ArrayText.getText();
			            String nxt=al.get(al.indexOf(itm)+1);
			            ArrayText.setText( nxt);
			            if(nxt==al.get(al.size()-1))
			            {
			            ArrayText.setText( nxt);
			            NextButton.setEnabled(false);
			            }
			            else
			            	ArrayText.setText( nxt);
				}
					
				}
			
		});
		NextButton.setFont(new Font("Tahoma", Font.BOLD, 20));
		NextButton.setBounds(369, 137, 128, 39);
		getContentPane().add(NextButton);
		
		JButton LastButton = new JButton("Last");
		LastButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(e.getSource() == LastButton) {
					
					
				            String last = al.get(al.size()-1);  
				            ArrayText.setText(last);
				           NextButton.setEnabled(false);
				}
			}
		});
		LastButton.setFont(new Font("Tahoma", Font.BOLD, 20));
		LastButton.setBounds(546, 137, 128, 39);
		getContentPane().add(LastButton);
		
		JButton FirstButton = new JButton("First");
		FirstButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(e.getSource() == FirstButton) {
					
				            String first = al.get(0);   
				            ArrayText.setText( first);
				           PreviousButton.setEnabled(false);   
					 
				}
			}
		});
		FirstButton.setFont(new Font("Tahoma", Font.BOLD, 20));
		FirstButton.setBounds(44, 137, 128, 39);
		getContentPane().add(FirstButton);
	
	 
			
		}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayFrameNew frame = new ArrayFrameNew();
        frame.setTitle("Login Form");
        frame.setVisible(true);
        frame.setBounds(10, 10, 700, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);


	}
		}
