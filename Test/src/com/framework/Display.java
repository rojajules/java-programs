package com.framework;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JButton;

public class Display extends JFrame {
	private JTextField NameText;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	public Display() {
		getContentPane().setLayout(null);
		
		JLabel Name = new JLabel("Entername");
		Name.setFont(new Font("Tahoma", Font.BOLD, 16));
		Name.setBounds(60, 26, 145, 26);
		getContentPane().add(Name);
		
		NameText = new JTextField();
		NameText.setFont(new Font("Tahoma", Font.BOLD, 16));
		NameText.setBounds(269, 32, 186, 19);
		getContentPane().add(NameText);
		NameText.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setBounds(94, 115, 45, -40);
		getContentPane().add(lblNewLabel);
		
		JLabel Address = new JLabel("Address");
		Address.setFont(new Font("Tahoma", Font.BOLD, 16));
		Address.setBounds(60, 72, 145, 26);
		getContentPane().add(Address);
		
		JTextArea AddressText = new JTextArea();
		AddressText.setBounds(269, 75, 397, 103);
		getContentPane().add(AddressText);
		
		JRadioButton MarrRadio = new JRadioButton("Married");
		buttonGroup.add(MarrRadio);
		MarrRadio.setFont(new Font("Tahoma", Font.BOLD, 16));
		MarrRadio.setBounds(333, 250, 103, 21);
		getContentPane().add(MarrRadio);
		
		JRadioButton UnMarrRadio = new JRadioButton("unmarried");
		buttonGroup.add(UnMarrRadio);
		UnMarrRadio.setFont(new Font("Tahoma", Font.BOLD, 16));
		UnMarrRadio.setBounds(471, 250, 123, 21);
		getContentPane().add(UnMarrRadio);
		
		JRadioButton IndianRadio = new JRadioButton("Indian");
		buttonGroup_1.add(IndianRadio);
		IndianRadio.setFont(new Font("Tahoma", Font.BOLD, 16));
		IndianRadio.setBounds(94, 312, 103, 21);
		getContentPane().add(IndianRadio);
		
		JRadioButton NRIradio = new JRadioButton("NRI");
		buttonGroup_1.add(NRIradio);
		NRIradio.setFont(new Font("Tahoma", Font.BOLD, 16));
		NRIradio.setBounds(299, 312, 103, 21);
		getContentPane().add(NRIradio);
		
		JLabel hobbies = new JLabel("Hobbies");
		hobbies.setFont(new Font("Tahoma", Font.BOLD, 16));
		hobbies.setBounds(72, 356, 109, 26);
		getContentPane().add(hobbies);
		
		JCheckBox ReadingCheckbox = new JCheckBox("Reading");
		ReadingCheckbox.setFont(new Font("Tahoma", Font.BOLD, 16));
		ReadingCheckbox.setBounds(191, 361, 93, 21);
		getContentPane().add(ReadingCheckbox);
		
		JCheckBox SwimmingCheckbox = new JCheckBox("Swimming");
		SwimmingCheckbox.setFont(new Font("Tahoma", Font.BOLD, 16));
		SwimmingCheckbox.setBounds(318, 361, 123, 21);
		getContentPane().add(SwimmingCheckbox);
		
		JCheckBox RunningCheckbox = new JCheckBox("Running");
		RunningCheckbox.setFont(new Font("Tahoma", Font.BOLD, 16));
		RunningCheckbox.setBounds(479, 361, 135, 21);
		getContentPane().add(RunningCheckbox);
		
		JButton ShowMeButton = new JButton("ShowMe");
		ShowMeButton.setFont(new Font("Tahoma", Font.PLAIN, 16));
		ShowMeButton.setBounds(349, 427, 123, 21);
		getContentPane().add(ShowMeButton);
		
		JLabel MaratialStatus = new JLabel("MaratialStatues");
		MaratialStatus.setFont(new Font("Tahoma", Font.BOLD, 16));
		MaratialStatus.setBounds(72, 241, 151, 30);
		getContentPane().add(MaratialStatus);
		ShowMeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 if (e.getSource() == ShowMeButton) {
					 String name;
					 String address;
					 name = NameText.getText();
					 address=AddressText.getText();
					 MarrRadio.setActionCommand("Married");
				        UnMarrRadio.setActionCommand("UnMarried");
				        IndianRadio.setActionCommand("Indian");
				        NRIradio.setActionCommand("NRI");
				        String Mart=buttonGroup.getSelection().getActionCommand();;
				        String national=buttonGroup_1.getSelection().getActionCommand();
				        JOptionPane.showInputDialog(this, " Name: "+name+"\n Address: "+address+"\n Martial status: "+Mart+"\n Nationality: "+national);
				 }
			}
		});
		 
	}
	 
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Display df=new Display();
		df.setTitle("Display Details");
        df.setVisible(true);
        df.setBounds(10, 10, 700, 700);
        df.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        df.setResizable(false);
	}
}
