package com.framework;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class EnquiryFrame  extends JFrame implements ActionListener {
Container container = getContentPane();
	
    JLabel lblNewLabel_2 = new JLabel("Nationality");
	JLabel name = new JLabel("Name");
	JLabel lblNewLabel_1 = new JLabel("Address");
	JTextField nametxt = new JTextField();
	JButton btshowme = new JButton("Show me");
	JTextArea addr = new JTextArea();
	JLabel lblNewLabel = new JLabel("Martial Status");
	 JRadioButton rdmar = new JRadioButton("Married");
	
	 JRadioButton  rdunmar = new JRadioButton("Unmarried");
	 JRadioButton rdind = new JRadioButton("Indian");
	 JRadioButton rdnri = new JRadioButton("NRI");
	 JLabel lblNewLabel_3 = new JLabel("Hobbies");
	 JCheckBox ckread = new JCheckBox("Reading books");
	 JCheckBox ckplay = new JCheckBox("Playing games");
	  JCheckBox ckgard = new JCheckBox("Gardening");
	 ButtonGroup buttonGroup = new ButtonGroup();
	 ButtonGroup buttonGroup_1 = new ButtonGroup();
	 JButton showme = new JButton("Show me");
	 
	public void setLocationAndSize() {
		name.setBounds(10, 70, 134, 35);
		lblNewLabel_1.setBounds(10, 111, 90, 21);
		nametxt.setBounds(154, 77, 86, 20);
		
		  addr.setBounds(154, 109, 192, 60);
		  rdmar.setBounds(154, 196, 109, 23);
	        lblNewLabel.setBounds(10, 186, 90, 21);
	        rdunmar.setBounds(259, 196, 109, 23);
	        lblNewLabel_2.setBounds(10, 242, 90, 23);
	        rdind.setBounds(154, 242, 109, 23);
	        rdnri.setBounds(259, 242, 109, 23);
	        lblNewLabel_3.setBounds(10, 293, 46, 14);
	        ckread.setBounds(154, 289, 97, 23);
	        ckplay.setBounds(272, 289, 97, 23);
	        ckgard.setBounds(386, 289, 97, 23);
	        btshowme.setBounds(174, 342, 89, 23);
	        rdmar.setActionCommand("Married");
	        rdunmar.setActionCommand("UnMarried");
	        rdind.setActionCommand("Indian");
	        rdnri.setActionCommand("NRI");
    }
	 EnquiryFrame() {
	        setLayoutManager();
	        setLocationAndSize();
	        addComponentsToContainer();
	        addActionEvent();

	    }
	 public void setLayoutManager() {
	        container.setLayout(null);
	    }
	 public void addActionEvent() {
		 btshowme.addActionListener(this);
		 
	    }
	 public void addComponentsToContainer() {
	        container.add(name);
	        container.add(lblNewLabel_1);
	        container.add(nametxt);
	        container.add(lblNewLabel_2);
	        container.add(addr);
	        container.add(lblNewLabel_2);
	        container.add(btshowme);
	       container.add(rdmar );
	       container.add(rdunmar);
	       container.add(rdind );
	       container.add(rdnri );
	       container.add(lblNewLabel_3 );
	       container.add(ckread  );
	       container.add( ckplay);
	       container.add(ckgard );
	       buttonGroup.add(rdmar);
	        buttonGroup.add(rdunmar);
	        buttonGroup_1.add(rdind);
	       buttonGroup_1.add(rdnri);
	       
	       
	    }

	
	@Override
	public void actionPerformed(ActionEvent e) {
		 String name= nametxt.getText();
         String add= addr.getText();
         String Mart=buttonGroup.getSelection().getActionCommand();;
         String national=buttonGroup_1.getSelection().getActionCommand();
         String ckhobby=" ";
         if(ckplay.isSelected())
        	 ckhobby=ckhobby+ckplay.getText();
         if(ckgard.isSelected())
        	 ckhobby=ckhobby+ckgard.getText();
         if(ckread.isSelected())
        	 ckhobby=ckhobby+ckread.getText();
		 if (e.getSource() == btshowme) {
	          
	            JOptionPane.showMessageDialog(this, " Name: "+name+"\n Address: "+add+"\n Martial status: "+Mart+"\n Nationality: "+national+"\n Hobbies: "+ckhobby);
	            
	        }
	
	        
	}
}
