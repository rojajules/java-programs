package com.framework;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Arithmetic extends JFrame {
	private JTextField number1text;
	private JTextField textField;
	public Arithmetic() {
		getContentPane().setLayout(null);
		
		JLabel number1 = new JLabel("Enter no1");
		number1.setBounds(58, 54, 151, 32);
		number1.setFont(new Font("Tahoma", Font.BOLD, 20));
		getContentPane().add(number1);
		
		number1text = new JTextField();
		number1text.setBounds(282, 63, 115, 23);
		getContentPane().add(number1text);
		number1text.setColumns(10);
		
		JLabel number2 = new JLabel("Enter no2");
		number2.setBounds(62, 106, 150, 24);
		number2.setFont(new Font("Tahoma", Font.BOLD, 18));
		getContentPane().add(number2);
		
		textField = new JTextField();
		textField.setBounds(285, 102, 114, 28);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton ButtonAdd = new JButton("ADD");
		ButtonAdd.setBounds(83, 225, 85, 32);
		ButtonAdd.setFont(new Font("Tahoma", Font.BOLD, 17));
		getContentPane().add(ButtonAdd);
		ButtonAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(e.getSource() == ButtonAdd) {
					float number1;
					float number2;
					number1= Float.parseFloat(number1text.getText());
					number2= Float.parseFloat(textField.getText());
					float add;
					add=(number1+number2);
					JOptionPane.showInputDialog(add, "the addition of two numbers is"+add);
				}
			}
		});
		
		JButton ButtonSub = new JButton("SUB");
		ButtonSub.setBounds(255, 229, 85, 30);
		ButtonSub.setFont(new Font("Tahoma", Font.BOLD, 16));
		getContentPane().add(ButtonSub);
		ButtonSub.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e1) {
				if(e1.getSource() == ButtonSub) {
					float number1;
					float number2;
					number1= Float.parseFloat(number1text.getText());
					number2= Float.parseFloat(textField.getText());
					float sub;
					sub=(number1-number2);
					JOptionPane.showInputDialog(sub, "the subraction of two numbers is"+sub);
				}
			}
		});
		
		JButton ButtonMul = new JButton("MUL");
		ButtonMul.setBounds(403, 225, 85, 33);
		ButtonMul.setFont(new Font("Tahoma", Font.BOLD, 16));
		ButtonMul.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e2) {
				if(e2.getSource() == ButtonMul) {
					float number1;
					float number2;
					number1= Float.parseFloat(number1text.getText());
					number2= Float.parseFloat(textField.getText());
					float mul;
					mul=number1*number2;
					JOptionPane.showInputDialog(mul, "the multiplication of two numbers is "+mul);
				}
			}
		});
		getContentPane().add(ButtonMul);
		
		JButton ButtonDiv = new JButton("DIV");
		ButtonDiv.setBounds(581, 226, 85, 32);
		ButtonDiv.setFont(new Font("Tahoma", Font.BOLD, 16));
		getContentPane().add(ButtonDiv);
		ButtonDiv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e3) {
				if(e3.getSource() == ButtonDiv) {
					
					float number1;
					float number2;
					number1= Float.parseFloat(number1text.getText());
					number2= Float.parseFloat(textField.getText());
					float div;
					div=(number1/number2);
					JOptionPane.showInputDialog(div, "the multiplication of two numbers is "+div);
				}
			}
		});
	}
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Arithmetic af=new Arithmetic();
		af.setTitle("Arithmetic Operation");
        af.setVisible(true);
        af.setBounds(10, 10,800 , 800);
        af.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        af.setResizable(false);

	}

}
