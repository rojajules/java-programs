package serialization.basics;

public class Manager extends Employee{
	public float salary;
	public String jobdesc;
	public Manager(int empid, String empname, String password, float salary, String jobdesc) {
		super(empid, empname, password);
		this.salary = salary;
		this.jobdesc = jobdesc;
	}
	@Override
	public String toString() {
		return "Manager [salary=" + salary + ", jobdesc=" + jobdesc + ", empid=" + empid + ", empname=" + empname
				+ ", password=" + password + "]";
	}
	

}
