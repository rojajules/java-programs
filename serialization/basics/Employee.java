package serialization.basics;

import java.io.Serializable;

public class Employee implements Serializable{
	public int empid;
	public String empname;
	transient public String password;
	public Employee(int empid, String empname, String password) {
		super();
		this.empid = empid;
		this.empname = empname;
		this.password = password;
	}
	@Override
	public String toString() {
		return "Employee [empid=" + empid + ", empname=" + empname + "]";
	}
	

}
