create table teacher(

empid int not null primary key,

empname varchar(25) not null,

salary float)



desc student1


insert into student1 values (1,"nandini",97)

insert into student1 values (2,"vandhana",98) ,

(3,"Afiya",88)


insert into student1 values 
(4,"roja",89) ,

(5,"lincy",87);

insert into student1 values (6,"praveen",85);



select * from student1;


select * from student1 where percentage >=90
;

select * from student1 where name like 'r%';


select name from student1 where percentage >=80 and percentage !=89
;

select name from student1 where name like 'n%' or name like 'v%'
;

select length(name) , name from student1
;

select length(name) , upper(name) from student1;


select lpad(name , 10 , "*")  from student1;


select rpad(name , 10 , "*")  from student1;


select name , instr(name,'a') from student1
;

select  max(percentage) from student1;


select  min(percentage) from student1;

select  avg(percentage) from student1;

delete from student1;

delete from student1 where rollno=1;


truncate  student1;


insert into student1 values (4,"roja",89) ,

(5,"lincy",87);


update student1 set percentage=95 where rollno=4;

alter table student1 add email varchar(50);

desc student1;


alter table student1 drop column percentage;

alter table student1 modify email varchar(100);

drop table student1;

create table student( 
rollno int primary key auto_increment,
name varchar(50) not null,
mobileno int unique,
empno int,
age int,
check(age>0),
foreign key(empno) references teacher(empno)
);

insert into teacher values(102,"radha",95000);

insert into student(name,mobileno,age,empno)
values("praveena",12365,10,102);

create table course(
courseid int(4) primary key,
coursename varchar(20),
duration int,fees float(7,2));

create table student1(
studid int(4) primary key,
firstname varchar(20),
lastname varchar(20),
street varchar(20),
city varchar(20),
DOB date);

create table registration(
courseid int(4),
studid int(4),
DOJ date,
foreign key(courseid) references course(courseid),
foreign key(studid) references student1(studid));

insert into course values(1001,"Java",4,5000),
(1002,"C++",2,4000),
(1003,"Linux and C",3,4000),
(1004,"Oracle",2,3000);

alter table student1  auto_increment=3001;

insert into student1(studid,firstname,lastname,street,city,DOB)
values
(3004,"Priya","Shankar","Gandhipuram","Coimbatore",'1990-01-22');

select * from course;

insert into registration(courseid,studid,DOJ)
values(1002,3003,'2011-04-18');

delete from student1 where studid=1000;

alter table course add constraint c1 check(fees>0);

update student1 set age=Year(curdate())-year(DOB);

update course set fees=fees-500 where duration<=3;

delete from student1 
where lastname like "Shankar" and city like "Coimbatore";

alter table registration add constraint c1 foreign key 'reg_studid' references 'student1'('studid') on delete cascade;

alter table registration rename column studid to reg_studid;

select * from student1;

alter table registration add foreign key(reg_studid) references student1(studid) on delete cascade;

select * from course where coursename="C++";

select * from course where fees>4000;

select * from student1 where month(DOB)>=01 and month(DOB)<=09 ;

select * from student1 where age=(select max(age) from student1);