package com.classmethods;

public class FirstClassMethod {
	public void test()
	{
		System.out.println("in testmethod");
	}
	private void private_test()
	{
		System.out.println("in private test method");
	}
	void default_test()
	{
		System.out.println("in default test method");
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FirstClassMethod f1=new FirstClassMethod();
		f1.test();
		f1.private_test();
		f1.default_test();
		

	}

}
