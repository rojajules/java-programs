package com.Serialization;

import java.io.Serializable;

public class Employee implements Serializable{
	public int empid;
	public String empname;
	public float salary;
	public Employee(int empid, String empname, float salary) {
		super();
		this.empid = empid;
		this.empname = empname;
		this.salary = salary;
	}
	@Override
	public String toString() {
		return "Employee [empid=" + empid + ", empname=" + empname + ", salary=" + salary + "]";
	}
	
}
