package com.Serialization;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class EmpMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Employee e=new Employee(100,"rudra",23456);
		try {
			//serialization
			FileOutputStream out=new FileOutputStream("E:\\emp.txt");
			ObjectOutputStream o=new ObjectOutputStream(out);
			o.writeObject(e);
			o.flush();
			o.close();
			out.close();
			System.out.println("done...serialization");
			//deserialization
			FileInputStream in=new FileInputStream("E:\\emp.txt");
			ObjectInputStream oi=new ObjectInputStream(in);
			Employee e1=(Employee) oi.readObject();
			System.out.println(e1);
			System.out.println("done...deserialization");
			oi.close();
			in.close();
			
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

}
