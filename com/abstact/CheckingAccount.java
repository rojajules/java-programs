package com.abstact;

public class CheckingAccount extends Account {
	private int numberOfChecksUsed;

	public CheckingAccount(String id, double balance, int numberOfChecksUsed) {
		super(id, balance);
		this.numberOfChecksUsed = numberOfChecksUsed;
	}

	@Override
	public boolean withdraw(double amount) {
		// TODO Auto-generated method stub
		if(amount<0)
			return false;
		else
			balance--;
			return true;
	}

	@Override
	public void deposit(double amount) {
		// TODO Auto-generated method stub
		
	}
	public void resetChecksUsed() {
		numberOfChecksUsed=0;
		System.out.println(+numberOfChecksUsed); 
	}
	public int getChecksUsed() {
		return numberOfChecksUsed;
	}
	public boolean withdrawUsingCheck(double amount) {
		if(amount>10)
			return true;
		else
			return false;
	}

}
