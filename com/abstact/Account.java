package com.abstact;

public abstract class Account {
	protected String id;
	protected double balance;
	public Account(String id, double balance) {
		super();
		this.id = id;
		this.balance = balance;
	}
	public String getid() {
		return id;
	}
	public double getbalance(){
		return balance;
		
	}
	@Override
	public String toString() {
		return "Account [id=" + id + ", balance=" + balance + "]";
	}
	public abstract boolean withdraw(double amount);
	public abstract void deposit(double amount);

}
