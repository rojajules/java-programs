package com.files;

import java.io.FileInputStream;
import java.io.FileOutputStream;

public class CopyFile {
	public static void main(String[] args) {

	    byte[] array = new byte[50];
	    try {
	      FileInputStream sourceFile = new FileInputStream("e:\\input.txt");
	      FileOutputStream destFile = new FileOutputStream("e:\\newfile.txt");
	      sourceFile.read(array);
	      destFile.write(array);
	      sourceFile.close();
	      destFile.close();
	      System.out.println("The input.txt file is copied to newFile.");
	    }
	    catch (Exception e) {
	      e.getStackTrace();
	    }
	  }

}
