package com.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileRe {
	public static void main(String[] args) {
		try {
		File f=new File("E://file1.txt");
		FileReader fr=new FileReader(f);
		BufferedReader br=new BufferedReader(fr);
		String s;
		while((s=br.readLine())!=null) {
			System.out.println(s);
		}
	}
		catch (FileNotFoundException e) {
			System.out.println("File doesn't exist");
			e.printStackTrace();
		}
		catch(IOException e1)
		{
			System.out.println("File can't be read");
			e1.printStackTrace();
		}
	}

}
