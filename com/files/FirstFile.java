package com.files;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FirstFile {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			FileInputStream in=new FileInputStream("E:\\file1.txt");
			int i=0;
			while((i=in.read())!=-1) {
				System.out.print((char)i);
			}
		}catch(FileNotFoundException e) {
			System.out.println("file doesnt exists");
			e.printStackTrace();
		}
		catch(IOException e1) {
			System.out.println("file cannot read");
			e1.printStackTrace();
		}

	}

}
