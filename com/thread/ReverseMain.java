package com.thread;

public class ReverseMain{
	public synchronized void reverse(String str) throws InterruptedException{
		char arr[]=str.toCharArray();		
		for(int i=arr.length-1;i>=0;i--)                 
		{  
		System.out.print(arr[i]); 
		Thread.sleep(1000);
		}  
		System.out.println( );
	}
	public static void main(String args[]) {
		ReverseMain obj=new ReverseMain();
		RevThread1 r1=new RevThread1(obj);
		RevThread2 r2=new RevThread2(obj);
		r1.start();
		r2.start();
	}

}
