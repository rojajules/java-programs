package com.thread;

public class Waiter implements Runnable{
		private Message msg;
		
		public Waiter(Message msg) {
			super();
			this.msg = msg;
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			String name=Thread.currentThread().getName();
			synchronized(msg){
				try {
					System.out.println(name+"waiting to get notified at time"+System.currentTimeMillis());
					msg.wait();
				}catch(InterruptedException e) {
					e.printStackTrace();
				}
			}
			System.out.println(name+"waiter to get notified at time"+System.currentTimeMillis());
			//process the message now
			System.out.println(name+"processed:"+msg.getMsg());
		}

		private void Synchronized (Message msg2) {
			// TODO Auto-generated method stub
			
		}
}
