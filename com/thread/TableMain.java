package com.thread;

public class TableMain {
	public synchronized void table(int num) throws InterruptedException{
		for(int i=1; i <= 10; i++)
        {
            System.out.println(num+" * "+i+" = "+num*i);
            Thread.sleep(1000);
        }
	}
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TableMain obj=new TableMain();
		TableThread1 t1=new TableThread1(obj);
		TableThread2 t2=new TableThread2(obj);
		t1.start();
		t2.start();
	}

}
