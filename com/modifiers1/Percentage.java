package com.modifiers1;

public class Percentage {
	public static char give_result(float percentage)
	{
		char result;
		if(percentage>=75)
			result='D';
		else if(percentage>=60 && percentage<75)
			result='1';		
		else if(percentage>=45 && percentage<60)
			result='S';	
		else if(percentage>=35 && percentage<45)
			result='T';	
		else 
			result='F';	
			return result;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		char marks=give_result(7);
		System.out.println("i got " +marks);
	}

}
