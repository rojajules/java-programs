package com.modifiers1;

public class SumDig {
	public static int sum_dig(int num)
	{
		int n,sum=0;
		while(num > 0)
        {
            n=num%10;
            sum=sum+n;
            num=num/10;
        }
		return sum;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int sumdigits=sum_dig(12345);
		System.out.println("the sum of the given number" +sumdigits);

	}

}
