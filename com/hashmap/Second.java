package com.hashmap;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class Second {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HashMap<Integer,String> map=new HashMap<Integer,String>();
		map.put(1, "prabha");
		map.put(2, "roja");
		map.put(3, "swathi");
		map.put(4, "moni");
		for(Entry<Integer,String> m:map.entrySet()) {
			System.out.println("key "+m.getKey()+ " value "+m.getValue());
		}
		for(Integer m:map.keySet()) {
			System.out.println("key "+m);
		}
		for( String m:map.values()) {
			System.out.println("values "+m);
	}
		Iterator<Map.Entry<Integer,String>> i=map.entrySet().iterator();
		while(i.hasNext()) {
			Map.Entry<Integer, String> itr=i.next();
			System.out.println("key "+itr.getKey()+ " value "+itr.getValue());
		}
}
}